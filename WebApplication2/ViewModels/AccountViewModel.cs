﻿using WebApplication2.Models;

namespace WebApplication2.ViewModels;

public class AccountViewModel
{
    public Account Data { get; set; }
    public List<Account>? DataList { get; set; }
    
    public String Json { get; set; }
}