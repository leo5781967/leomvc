﻿using WebApplication2.Interfaces;
using WebApplication2.Models;

namespace WebApplication2.Services;

public class ProfileService : IProfileService
{
    public static List<Account> Accounts = new List<Account>();
    
    public List<Account> Get()
    {
        return Accounts;
    }

    public void create(Account data)
    {
        Accounts.Add(data);
    }
}