﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers;

[Route("Titaner/[controller]/[action]")]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IHomeService _homeService;

    public HomeController(ILogger<HomeController> logger, IHomeService homeService)
    {
        _logger = logger;
        _homeService = homeService;
    }

    public IActionResult Index()
    {
        _homeService.Hello();
        
        var order = new Order(){name = "hello", MyProduct = new Product()};
        return View(order);
    }
    
    [HttpGet("Hello")]
    public IActionResult Privacy()
    {
        return View();
    }

    public IActionResult Save(Order order)
    {
        var modelStateIsValid = ModelState.IsValid;
        return View("Index", order);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}

public interface IHomeService
{
    void Hello();
}

public class HomeService : IHomeService
{
    public void Hello()
    {
        Console.WriteLine("123");
    }
}

public class Order
{
    [Required]
    public string name { get; set; }
    public Product MyProduct { get; set; }
}

public class Product
{
    public string ProductName { get; set; }
}