﻿using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Interfaces;
using WebApplication2.Models;
using WebApplication2.ViewModels;
using Newtonsoft.Json;

namespace WebApplication2.Controllers;

//
// 建立一個記帳的網頁
// 要有 Create 與 View 可新增&&檢視 並且在同一個畫面
// 對Create欄位做驗證
// 需要有 金額-Amount 消費日-Date 購買物品-Remark
// 利用 static 存資料
// 要利用 DI 的方式做資料的新增/取得
// 使用 ViewModel方式
// 開一隻API 為 [api/records] 執行後將有的資料轉成 JSON格式(GET)
// 

public class ProfileController : Controller
{
    private readonly IProfileService _profileService;
    public ProfileController(IProfileService profileService)
    {
        _profileService = profileService;
    }
    // GET
    public IActionResult Index()
    {
        AccountViewModel data = new AccountViewModel();
        data.DataList = _profileService.Get();
        return View(data);
    }

    [HttpPost]
    public IActionResult Create(Account data)
    {
        _profileService.create(data);

        return RedirectToAction("Index");
    }

    [Route("/api/records")]
    public IActionResult Json()
    {
        AccountViewModel data = new AccountViewModel();
        data.DataList = _profileService.Get();
        data.Json = JsonConvert.SerializeObject(data.DataList);
        return View(data);
    }

    public IActionResult Privacy()
    {
        return View();
    }
}