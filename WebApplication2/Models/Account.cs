﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models;

public class Account
{
    [DisplayName("金額")]
    [Required]
    public int? Amount { get; set; }
    [DisplayName("消費日期")]
    [Required]
    public DateTime Date { get; set; }
    [DisplayName("物品")]
    [Required]
    public string? Remark { get; set; }
}