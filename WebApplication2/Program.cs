using Microsoft.AspNetCore.Mvc;
using WebApplication2.Controllers;
using WebApplication2.Interfaces;
using WebApplication2.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

var service = builder.Services;
service.AddTransient<IHomeService, HomeService>();
service.AddTransient<IProfileService, ProfileService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "profileRoute",
    pattern: "profile/{action}",
    defaults: new { controller = "Profile", action = "Index" }
);

app.MapControllerRoute(
    name: "default",
    pattern: "Titaner/{controller=Home}/{action=Index}/{id?}");

app.Run();