﻿using WebApplication2.Models;

namespace WebApplication2.Interfaces;

public interface IProfileService
{
    List<Account> Get();
    void create(Account data);
}